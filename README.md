# CS425_MP1

### Mai Pham (maitp2) and Trang Do(trangtd2)

Task: Distributed Log Querier (detail in MP1.CS415.FA22.pdf)

## Instructions

### Run servers and client
1. ssh to VM: ```ssh <NETID>@fa22-cs425-NNXX.cs.illinois.edu```
2. ```git clone https://gitlab.engr.illinois.edu/trangtd2/cs425_mp1.git```
3. cd to project dir
4. The program is compiled to two jar files: ```out/artifacts/grep_server_jar/grep.jar```(server execution jar file)
and ```out/artifacts/grep_client_jar/grep.jar```(client execution jar file) <br>
In order to run server on single machine/VM, run the jar file:
```
java -jar out/artifacts/grep_server_jar/grep.jar -p 5000 
```
User can replace 5000 by any port number
4. Set configFile in ```files/config.txt```, fill out number of servers line by line. Config file has format:
```
<host> <port> <dir of log file>
```
5. Run client
```
java -jar out/artifacts/grep_client_jar/grep.jar -cf <configFile> --command "grep -c <pattern/regex>" 
```

### Unit test
User run unit test in ```GrepTest.java```
This test is test for 10 VMs with config file is in ```files/config.txt```, therefore it is required to keep all 10 
servers running and actual test result is in ```files/test_result```

### Note
1. ``` files/bash/send_log.sh```: use to push demo data (provided demo data) to all 10 VMs
2. ```files/bash/send_jars.sh```: use to send all client execution jar and server execution jar to all 10 VMs
3. ```files/bash/run_jars.sh```: use to run all servers from local machine
4. ```files/bash/kill_process.sh```: use to kill server processes running in all 10 VMs
