package cs425.mp1;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.io.IOException;

public class Client {
    public static void main(String[] args) throws IOException {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("Client")
                .defaultHelp(true)
                .description("Run distributed grep.");
        /*
        Config file is a text file has format: <server_host> <server_port> <log_file>
         */
        parser.addArgument("-cf","--configFile").nargs("?")
                .help("Path of the config file containing list of host and its port");
        parser.addArgument("--command").nargs("*")
                .help("Put command of grep: grep <grep_options> <string_to_match>");

        /* Read input arguments  */
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }


        /* Read config file */
        String configFile = ns.getString("configFile");
        String pattern = String.join(" ", ns.getList("command"));
        ClientHandler.start(configFile, pattern);

    }
}
