package cs425.mp1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ClientHandler {

    public static ArrayList<ResultInfo> start(String confFile, String grepCommand) throws IOException {
        /* Read server hosts, ports, and log files from config files */
        FileReader file = new FileReader(confFile);
        ArrayList<ServerConf> servers = new ArrayList<>();
        String line;
        BufferedReader reader = new BufferedReader(file);
        int totalLines = 0;

        while (true) {
            line = reader.readLine();
            if (line != null) {
                String[] s = line.split(" ");

                String server = s[0].trim();
                int port = Integer.parseInt(s[1].trim());
                String logFile = s[2].trim();

                servers.add(new ServerConf(server, port, logFile));
            } else {
                break;
            }
        }
        reader.close();

        /*
        Creating threads based on number of servers
         */
        long start = System.currentTimeMillis();

        int n = servers.size();
        ArrayList<GrepThread> threads = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            GrepThread t = new GrepThread(servers.get(i), grepCommand);
            threads.add(t);
            t.start();
        }

        /* End threads */
        ArrayList<ResultInfo> results = new ArrayList<>();
        for (GrepThread t: threads) {
            try {
                t.join();
                if (t.getResult() != null){
                    results.add(t.getResult());
                    totalLines += t.getResult().getMatches();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long end = System.currentTimeMillis();

        System.out.println("Total lines: " + totalLines);
        System.out.println("Query Time: " + (end - start) + " ms" );

        results.add(new ResultInfo("total", "total", totalLines));

        return results;
    }
}
