package cs425.mp1;

import java.util.List;

public class ResultInfo implements Comparable<ResultInfo> {
    private String host;
    private String logName;
    private int matches;

    public ResultInfo(String host, String logName, int matches) {
        this.host = host;
        this.logName = logName;
        this.matches = matches;
    }

    public String getHost() {
        return host;
    }

    public String getLogName() {
        return logName;
    }

    public int getMatches() {
        return matches;
    }

    public void print(){
        System.out.println(host + ":" + logName + " --- " + matches + " matches");
    }


    @Override
    public int compareTo(ResultInfo other) {

        String otherLogId = other.getLogName().replace("vm", "").replace(".log", "");
        String logId = logName.replace("vm", "").replace(".log", "");

        //ascending order
        return Integer.parseInt(logId.trim()) - Integer.parseInt(otherLogId.trim());

    }
}
