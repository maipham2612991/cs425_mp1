package cs425.mp1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Server {
    private int port;
    private Socket socket   = null;
    private ServerSocket server   = null;

    public Server(int port) {
        this.port = port;
        try {
            /* Establish socket to client */
            server = new ServerSocket(port);
            System.out.println("Server start listening on port "+ port);

            while (true){
                socket = server.accept();
                System.out.println("Client connected");

            /*
            Input command from client and call Grep class to execute, then return output to output stream
             */
                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                try {
                    String mess = (String) inputStream.readObject();

                /*
                2 case: 1 case is generate log, 1 case is command grep
                 */
                    String res;

                    String grepCommand = mess.trim();
                    res = String.join("\n", Grep.run_cmd(grepCommand));
                    System.out.println(res);

                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeObject(res);

                } catch (IOException | ClassNotFoundException e) {
                    System.err.println("Error reading message");
                    e.printStackTrace();
                }
            }

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        if (socket != null) {
            socket.close();
        }
    }
}
